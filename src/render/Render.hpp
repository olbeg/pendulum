#ifndef RENDER_H
#define RENDER_H

#include <utility>

class PixelPoint {
public:
    int x();
    int y();

private:
    int _x;
    int _y;
};

class Render
{

public:
    virtual void drawRectangle(PixelPoint  p1, Number w, Number h) = 0;
    virtual void drawLine(PixelPoint  p1, PixelPoint p2) = 0;
    virtual void drawEllipse(PixelPoint p1, Number r1, Number r2) = 0;
};

#endif // RENDER_H

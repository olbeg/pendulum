#ifndef SIMULATION_HPP
#define SIMULATION_HPP

// qt
#include <QWidget>
#include <QPaintEvent>
#include <QWheelEvent>

// self
#include <world/World.hpp>

class Simulation : public QWidget {
  Q_OBJECT
public:
  Simulation(QWidget* parent = 0);
public:
  // world
  virtual void paintEvent(QPaintEvent* e);
  // observer
  virtual void wheelEvent(QWheelEvent* e);
  virtual void mouseMoveEvent(QMouseEvent* e);
  virtual void mousePressEvent(QMouseEvent* e);
  virtual void mouseReleaseEvent(QMouseEvent* e);
private:
  World mWorld;
};

#endif // SIMULATION_HPP

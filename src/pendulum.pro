#-------------------------------------------------
#
# Project created by QtCreator 2015-12-04T22:30:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pendulum
TEMPLATE = app

CONFIG   += c++14

SOURCES += \
    main.cpp\
    Simulation.cpp \
    world/World.cpp \
    world/Entity.cpp \
    world/Math.cpp  \
    render/Render.cpp \

HEADERS  += \
    Simulation.hpp \
    world/World.hpp \
    world/Entity.hpp \
    world/Math.hpp \
    render/Render.hpp \

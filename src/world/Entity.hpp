#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <memory>
#include <list>

#include "Math.hpp"
#include "render/Render.hpp"

class Entity {
    public: 
        using Ptr = std::shared_ptr<Entity>;
        using WeakPtr = std::weak_ptr<Entity>;
        using Attachments = std::list<Entity::WeakPtr>;

        void attach(Ptr other);
        void dettach(Ptr other);
        const Attachments& attachments() const;

        virtual void update(Time time) = 0;
        virtual void draw(const Render &p) const = 0;

        void setPosition(Point p);
        Point position() const;
    public:
        void setVelocity(Point v);
        void setAcceleration(Number a);
        Point velocity() const;
        Number acceleration() const;
        Point freeFall() const;
    private:
        Point mVelocity;
        Number mAcceleration;
        Attachments mLinks;
};

#endif

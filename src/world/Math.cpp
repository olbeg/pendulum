#include "Math.hpp"

Point::Point(Number x, Number y)
: mX(x)
, mY(y) {
  //
}

Number Point::x() const {
  return mX;
}

Number Point::y() const {
  return mY;
}

Point Point::operator-(const Point& other) const {
  return { this->x() - other.x(), this->y() - other.y() };
}

Point Point::operator+(const Point& other) const {
  return { this->x() + other.x(), this->y() + other.y() };
}



#include <algorithm>
#include "World.hpp"

using namespace std;

World::World() {

}

void World::draw(const Render &p) const {
    for_each(begin(mEntities), end(mEntities), [&p](Entity::Ptr e) {
        e->draw(p);
    });
}

void World::update(Time time) {
    for_each(begin(mEntities), end(mEntities), [&time](Entity::Ptr e) {
        e->update(time);
    });
}

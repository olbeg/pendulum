#include "Entity.hpp"

void Entity::attach(Ptr rhs) {
    mLinks.push_back(rhs);
}

void Entity::dettach(Entity::Ptr other) {

}

const Entity::Attachments &Entity::attachments() const{
    return mLinks;
}

void Entity::setVelocity(Point v) {
    mVelocity = v;
}

void Entity::setAcceleration(Number a) {
    mAcceleration = a;
}

Point Entity::velocity() const {
    return mVelocity;
}

Number Entity::acceleration() const {
    return mAcceleration;
}

Point Entity::freeFall() const {
    return { 0.f, 9.8 };
}


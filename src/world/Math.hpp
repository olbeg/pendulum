#ifndef MATH_HPP
#define MATH_HPP

// stl
#include <cmath>

using Number = float;
using Pixel  = int;
using Degree = int;
using Time   = float;

class Point {
public:
  Point (Number x, Number y);
public:
  Number x() const;
  Number y() const;
  Point operator-(const Point& other) const;
  Point operator+(const Point& other) const;
private:
  Number mX;
  Number mY;
};

namespace {

  inline Number pi() {
    return 3.14159265358979323846;
  }

  inline Number fromDeg(Degree deg) {
    return deg * pi() / 180.f;
  }

  inline Pixel toPixelX(Number x) {
    return x * 10;
  }

  inline Pixel toPixelY(Number y) {
    return y * 10;
  }

  inline Pixel toPixel(Number n) {
    return n * 10;
  }

}

#endif // MATH_HPP

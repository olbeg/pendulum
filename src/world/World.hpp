#ifndef WORLD_HPP
#define WORLD_HPP

#include <list>
#include "Entity.hpp"

class World {
    public: 
        using Entities = std::list<Entity::Ptr>;

        World();
        void draw(const Render &p) const;
        void update(Time time);
    private:
        Entities mEntities;
};

#endif /* WORLD_HPP */


// self
#include "Simulation.hpp"

// qt
#include <QWidget>
#include <QTimer>
#include <QPainter>

// tmp
#include <QDebug>


Simulation::Simulation(QWidget* parent)
: QWidget(parent)
, mWorld() {

  QTimer* loop = new QTimer(this);

  using Update = void (QWidget::*)(); // cast to correct overload
  connect(loop, &QTimer::timeout,
          this, static_cast<Update>(&QWidget::update));

  loop->start(100); 
}

void Simulation::paintEvent(QPaintEvent* e) {

  Q_UNUSED(e);

  Render r;
  mWorld.draw(r);
  mWorld.update(0.1f);
}

void Simulation::wheelEvent(QWheelEvent* e) {

}

void Simulation::mouseMoveEvent(QMouseEvent* e) {

}

void Simulation::mousePressEvent(QMouseEvent* e) {

}

void Simulation::mouseReleaseEvent(QMouseEvent* e) {

}

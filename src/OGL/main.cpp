#include <iostream>
#include <GL/glut.h>

GLdouble sphereX = 0.0, sphereY = 0.0;

void convertScreenToWordCoords(int x, int y) {
    GLint viewport[4]; //var to hold the viewport info
    GLdouble modelview[16]; //var to hold the modelview info
    GLdouble projection[16]; //var to hold the projection matrix info
    GLfloat winX, winY, winZ; //variables to hold screen x,y,z coordinates
    GLdouble worldX, worldY, worldZ; //variables to hold world x,y,z coordinates

    glGetDoublev(GL_MODELVIEW_MATRIX, modelview); //get the modelview info
    glGetDoublev(GL_PROJECTION_MATRIX, projection); //get the projection matrix info
    glGetIntegerv(GL_VIEWPORT, viewport); //get the viewport info

    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    winZ = 0;

    //get the world coordinates from the screen coordinates
    gluUnProject(winX, winY, winZ, modelview, projection, viewport,
                 &sphereX/*&worldX*/, &sphereY/*&worldY*/, &worldZ);
}

void onMouse(int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        switch (button) {
        case GLUT_LEFT_BUTTON:
            convertScreenToWordCoords(x,y);
            break;
        case GLUT_RIGHT_BUTTON:
            break;
        }
    }
}

void onReshape(int w, int h) {
    glViewport (0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective (60, (GLfloat)w / (GLfloat)h, 0.1, 100.0);
    glMatrixMode (GL_MODELVIEW);
}

void onDisplay() {
//    static double someMove = 0.0;

    glClearColor (0.0,0.0,0.0,1.0);
    glClear (GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0,0,-20);
    //glColor3f(1,1,1);

    glColor3f(0.7, 0.0, 0.0);
    glBegin(GL_QUADS);
    glNormal3f( 0.0,  0.0,  1.0);
    glVertex3d(-0.8,  0.0,  0.0);
    glVertex3d( 0.8,  0.0,  0.0);
    glVertex3d( 0.8,  0.8,  0.0);
    glVertex3d(-0.8,  0.8,  0.0);
    glEnd();

    glLineWidth(2);
    glColor3f(0.0, 0.7, 0.0);
    glBegin(GL_LINES);
    glVertex2f(sphereX*200, sphereY*200);
    glVertex2f(0.0, -4.0);
    glEnd();

//    glColor3f(0.7, 0.7, 0.7);
//    glPushMatrix();
//    glTranslated(sphereX*200, sphereY*200, 0.0);
//    glutWireSphere(0.82, 50, 50);
//    glPopMatrix();

    glutSwapBuffers();
}

int main (int argc, char * argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);

    glutInitWindowSize(1000, 1000);
    glutCreateWindow("Pendulum");

    glutReshapeFunc(onReshape);
    glutDisplayFunc(onDisplay);
    glutIdleFunc(onDisplay);
    glutMouseFunc(onMouse);

    glutMainLoop();

    return 0;
}

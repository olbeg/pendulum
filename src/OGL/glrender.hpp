#ifndef GLRENDER_HPP
#define GLRENDER_HPP

#include "../render/Render.hpp"

class GLRender : public Render {
public:
    GLRender();
    void drawRectangle(PixelPoint p1, Number w, Number h);
    void drawLine(PixelPoint  p1, PixelPoint p2);
    void drawEllipse(PixelPoint  p1, Number r1, Number r2);
};

#endif // GLRENDER_HPP

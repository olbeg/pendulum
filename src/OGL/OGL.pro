TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../render/Render.cpp \
    glrender.cpp

LIBS += -lGL -lGLU -lglut



include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    ../render/Render.hpp \
    glrender.hpp


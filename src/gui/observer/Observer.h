#ifndef OBSERVER_H
#define OBSERVER_H

#include <utility>

class Point;

namespace nsp {
  class Observer {
  public:
    Observer();
  public:
      // OBSV1
      auto origin() -> Point const;    // точка интерфейса окна совпадающая
                                // с началом координат World
      auto setScale(unsigned scale) -> void;
      auto getScale() const -> float; // масштаб мира
      auto setOrigin(const Point& origin) -> void;
      auto incrementWorldScale() -> void;
      auto decrementWorldScale() -> void;
      // OBSV2
      auto setTimeRate(float timeRate) -> void;

      auto getTimeRate() const -> float; // скорость течения времени
  private:
      float mTimeRate;
      float mScale;
    };
}
#endif // OBSERVER_H

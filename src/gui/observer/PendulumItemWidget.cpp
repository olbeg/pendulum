#include "PendulumItemWidget.h"
#include <QGridLayout>
#include <QLabel>
#include <QSpacerItem>

nsp::PendulumItemWidget::PendulumItemWidget(QWidget* parent, unsigned number)
  : QGroupBox(parent)
  , hookX(new QLineEdit())
  , hookY(new QLineEdit())
  , ballX(new QLineEdit())
  , ballY(new QLineEdit())
{
  QVBoxLayout* layout = new QVBoxLayout{};

  layout->addWidget(new QLabel { "Hook_" + QString::number(number) });
  layout->addWidget(createHookWidget());
  layout->addWidget(new QLabel { "Ball_" + QString::number(number) });
  layout->addWidget(createBallWidget());
  layout->addSpacerItem(new QSpacerItem(0, 20));

  this->setLayout(layout);
}


auto nsp::PendulumItemWidget::createHookWidget() -> QWidget* {
  QWidget* widget = new QWidget { };
  QGridLayout* layout = new QGridLayout { };
  layout->addWidget(new QLabel { "variable = x" }, 0, 0);
  layout->addWidget(hookX, 0, 1);
  layout->addWidget(new QLabel { "variable = y" }, 1, 0);
  layout->addWidget(hookY, 1, 1);
  widget->setLayout(layout);
  return widget;
}

auto nsp::PendulumItemWidget::createBallWidget() -> QWidget* {
  QWidget* widget =  new QWidget { };
  QGridLayout* layout = new QGridLayout { };
  layout->addWidget(new QLabel { "variable = x" }, 0, 0);
  layout->addWidget(ballX, 0, 1);
  layout->addWidget(new QLabel { "variable = y" }, 1, 0);
  layout->addWidget(ballY, 1, 1);
  widget->setLayout(layout);
  return widget;
}

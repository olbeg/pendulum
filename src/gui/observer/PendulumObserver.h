#ifndef PENDULUMOBSERVER_H
#define PENDULUMOBSERVER_H

#include <QWidget>

class PendulumObserver : public QWidget
  {
  Q_OBJECT

public:
  PendulumObserver(QWidget *parent = 0);
  ~PendulumObserver();
  };

#endif // PENDULUMOBSERVER_H

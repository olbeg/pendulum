#ifndef OBSERVERWIDGET_H
#define OBSERVERWIDGET_H

#include <QPushButton>
#include <QVBoxLayout>
#include "PendulumItemWidget.h"
#include <QLineEdit>

namespace nsp {
  class ObserverWidget : public QWidget {
    Q_OBJECT

  public:
    ObserverWidget(QWidget *parent = 0);
    ~ObserverWidget();

  private:
    auto createAddButton() -> QPushButton*;
    auto createScaleWidget() -> QWidget*;

  private:
    QVBoxLayout* mLayout;
  private:
    QPushButton* addButton;
    QLineEdit* scaleLineEdit;


  private:
    void addPendulum();

  private:
    unsigned pendulumsCount = 0;
    float mScale;
    float timeRate;
    };
  }

#endif // OBSERVERWIDGET_H

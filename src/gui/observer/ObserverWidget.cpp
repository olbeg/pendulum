#include "ObserverWidget.h"
#include <QLabel>


nsp::ObserverWidget::ObserverWidget(QWidget* parent)
  : QWidget(parent)
  , mLayout(new QVBoxLayout { } )
  , addButton(createAddButton())
  , scaleLineEdit(new QLineEdit { } )
{
  mLayout->addWidget(addButton);
  this->setLayout(mLayout);

  connect(addButton, &QPushButton::clicked, this, &addPendulum);
}

nsp::ObserverWidget::~ObserverWidget() {

}


auto nsp::ObserverWidget::createAddButton() -> QPushButton* {
  QPushButton* widget = new QPushButton();
  widget->setText("Add pendulum");
  return widget;
}

auto nsp::ObserverWidget::createScaleWidget() -> QWidget* {
  QWidget* widget = new QWidget { };
  QHBoxLayout* layout = new QHBoxLayout { };
  layout->addWidget(new QLabel { " 1 :" } );
  layout->addWidget(scaleLineEdit);
  widget->setLayout(layout);
  return widget;
}

auto nsp::ObserverWidget::addPendulum() -> void {
  QWidget* pendulumItem = new PendulumItemWidget(this, pendulumsCount);
  mLayout->addWidget(pendulumItem);
  ++pendulumsCount;
}





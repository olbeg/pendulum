#include "Observer.h"

nsp::Observer::Observer() {

}

auto nsp::Observer::origin() -> Point const {
  Point point;
  return point;
}

auto nsp::Observer::setScale(unsigned scale) -> void {
  mScale = scale;
}

auto nsp::Observer::getScale() const -> float {
  return mScale;
}

void nsp::Observer::setTimeRate(float timeRate) {
  mTimeRate = timeRate;
}

 auto nsp::Observer::getTimeRate() const -> float {
  return mTimeRate;
}


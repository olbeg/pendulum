#ifndef PENDULUMITEMWIDGET_H
#define PENDULUMITEMWIDGET_H

#include <QScrollArea>
#include <QPoint>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QGroupBox>

namespace nsp {
  class PendulumItemWidget : public QGroupBox {
  public:
    PendulumItemWidget(QWidget* parent, unsigned number);

  private:
    auto createHookWidget() -> QWidget*;
    auto createBallWidget() -> QWidget*;

  private:
    QLineEdit* hookX;
    QLineEdit* hookY;

    QLineEdit* ballX;
    QLineEdit* ballY;

  private:
    QPoint mHookPoint;
    QPoint mBallPoint;
    };
}

#endif // PENDULUMITEMWIDGET_H

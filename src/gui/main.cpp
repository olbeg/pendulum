#include "ObserverWidget.h"
#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  nsp::ObserverWidget w;
  w.show();

  return a.exec();
}

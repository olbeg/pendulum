#-------------------------------------------------
#
# Project created by QtCreator 2015-12-12T13:01:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PendulumObserver
TEMPLATE = app

CONFIG += c++14

QMAKE_CXXFLAGS += -std=c++1y

SOURCES += main.cpp\
        observer\ObserverWidget.cpp \
    observer\PendulumItemWidget.cpp \
    observer\Observer.cpp

HEADERS  += \
    observer\ObserverWidget.h \
    observer\PendulumItemWidget.h \
    observer\Observer.h
